import { connect } from 'react-redux'
import Review_post from '../components/Product_info/Review_post'
import {Rev} from "../actions/info-action";


const mapStateToProps = state => ({
    isLoggedIn: state.session.isLoggedIn,
    redirect: state.info.redirect
})

const mapDispatchToProps = dispatch => ({
    Rev: (params, cb) => dispatch(Rev(params, cb))
})

export default connect(mapStateToProps, mapDispatchToProps)(Review_post)
