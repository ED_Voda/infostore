import { connect } from 'react-redux'
import { getComment } from '../actions/info-action'
import Review from '../components/Product_info/Review'


const mapStateToProps = state =>
    ({
        currentProduct: state.info.currentProduct, error: state.info.errors
    });

const mapDispatchToProps = dispatch => {
    return {
        getComment: (id) => dispatch(getComment(id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Review)
