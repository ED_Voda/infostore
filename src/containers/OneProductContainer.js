import { connect } from 'react-redux'
import Production_info from '../components/Product_info/Production_info'


const mapStateToProps = state => {
    return { loading: state.info.loading, errors: state.info.errors};
}
const mapDispatchToProps = dispatch => {
    return {
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Production_info)
