import { connect } from 'react-redux'
import { logIn } from '../../actions/SessionActions'
import Login from '../../components/Auth/Login'

const mapStateToProps = state => ({
    errorMsgLog: state.session.errorMsgLog,
    redirect: state.session.redirect
})

const mapDispatchToProps = dispatch => ({
    logIn: (params, cb) => dispatch(logIn(params, cb)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Login)
