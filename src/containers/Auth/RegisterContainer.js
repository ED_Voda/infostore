import { connect } from 'react-redux'
import { Req } from '../../actions/SessionActions'
import Register from '../../components/Auth/Register'

const mapStateToProps = state => ({
    errorMsgReg: state.session.errorMsgReg,
    redirect: state.session.redirect
})

const mapDispatchToProps = dispatch => ({
    Req: (params, cb) => dispatch(Req(params, cb)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Register)
