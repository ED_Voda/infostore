import * as actionName from './actionName';
import {httpGet} from "../helpers/network";

export function Rev(params) {
    return dispatch => {
        dispatch({type: actionName.POST_START});
        return fetch(`http://smktesting.herokuapp.com/api/reviews/${params.id}`, {
            method: 'POST',
            headers : {
                'Authorization': `Token ${localStorage.getItem('token')}`,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body:JSON.stringify({rate:params.rate, text:params.text})
        }).then((res) => res.json())
            .then((data) =>  {
                if (data.success) {
                    dispatch({type: actionName.POST_SUCCES})
                    dispatch({type: actionName.POST_END})
                }else{
                    dispatch({
                        type: actionName.POST_FAILURE, payload: {errorMsg: 'Ошибка регестрации',},
                        error: true, // https://github.com/redux-utilities/flux-standard-action
                    })
                }

            })
            .catch((err)=>console.log(err));
    }
}

export const getComment = (id) => {
    return dispatch => {
        dispatch({type: actionName.INFO_GET_START})
        httpGet(`reviews/${id}`)
            .then(data => {
                console.log(data);
                    dispatch({type: actionName.INFO_GET_SUCCES, payload: data})
            })
                .catch(error => {
                        dispatch({type: actionName.INFO_GET_FAILURE, payload: error.data})
                    })
            }
};
