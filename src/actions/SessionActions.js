
import * as actionName from './actionName';

export const tokenCheck = () => {
    return {type: actionName.TOKEN_CHECK}
}

export function logIn(params) {
    return dispatch => {
        dispatch({type: actionName.LOG_START})
        return fetch('http://smktesting.herokuapp.com/api/login/', {
            method: 'POST',
            headers : {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body:JSON.stringify({username:params.username.toLowerCase(), password:params.password})
        }).then((res) => res.json())
            .then((data) =>  {

                if (data.success) {
                    dispatch({type: actionName.LOG_IN_SUCCES,
                        payload: { payload: data.token}
                        ,})
                    dispatch({type: actionName.LOG_IN_END})
                }else{
                    dispatch(
                        {type: actionName.LOG_IN_FAILURE,
                            payload: {errorMsg: 'Имя пользователя или пароль введены не верно',},
                            error: true, // https://github.com/redux-utilities/flux-standard-action
                    })
                }
            })
            .catch((err)=>console.log(err));
    }
}

export function Req(params) {
    return dispatch => {
        dispatch({type: actionName.REG_START});
        return fetch('http://smktesting.herokuapp.com/api/register/', {
            method: 'POST',
            headers : {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body:JSON.stringify({username:params.username.toLowerCase(), password:params.password})
        }).then((res) => res.json())
            .then((data) =>  {
                if (data.success)
                {
                    dispatch({type: actionName.REG_SUCCES, payload: { payload: data.token},})
                    dispatch({type: actionName.REG_IN_END})

                }else {
                    dispatch({
                        type: actionName.REG_IN_FAILURE, payload: {errorMsg: 'Ошибка регестрации',},
                        error: true, // https://github.com/redux-utilities/flux-standard-action
                    })
                }
            })
            .catch((err)=>console.log(err));
    }
}


export const logout = () => {
    return {type:actionName.LOG_OUT}
}
