export const TOKEN_CHECK = 'TOKEN_CHECK'

export const LOG_START = 'LOG_START'
export const LOG_IN_SUCCES = 'LOG_IN_SUCCES'
export const LOG_IN_FAILURE = 'LOG_IN_FAILURE'
export const LOG_IN_END = 'LOG_IN_END'


export const REG_START ='REG_START'
export const REG_SUCCES  ='REG_SUCCES'
export const REG_IN_FAILURE= 'REG_IN_FAILURE'
export const REG_IN_END= 'REG_IN_END'

export const POST_START = 'POST_START'
export const POST_SUCCES= 'POST_SUCCES'
export const POST_FAILURE= 'POST_FAILURE'
export const POST_END= 'POST_END'

export const INFO_GET_START = 'INFO_GET_START'
export const INFO_GET_SUCCES= 'INFO_GET_SUCCES'
export const INFO_GET_FAILURE= 'INFO_GET_FAILURE'

export const LOG_OUT = 'LOG_OUT'