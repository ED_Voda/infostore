import React, {Component} from 'react'
import {connect} from 'react-redux'
import { Switch, Route, Redirect,withRouter } from 'react-router-dom'
import LoginContainer from './containers/Auth/LoginContainer'
import './App.css'
import Home from "./components/Home";
import OneProductContainer from "./containers/OneProductContainer";
import RegisterContainer from "./containers/Auth/RegisterContainer";
import Header from "./components/Header";
import Logout from "./components/Auth/Logout"
import { logout,tokenCheck } from './actions/SessionActions'


export class App extends Component {

    componentDidMount = () => {
        this.props.tokenCheck()
    }

    render() {
        const NotloggedRoutes =
                    <Switch>
                        <Route exact path="/" component={Home}/>
                        <Route path="/login" component={LoginContainer}/>
                        <Route path="/products/:id" component={OneProductContainer}/>
                        <Route path="/Register" component={RegisterContainer}/>
                        <Redirect to='/'/>
                    </Switch>

        const loggedRoutes = <Switch>
            <Route exact path="/" component={Home}/>
            <Route path="/login" component={LoginContainer}/>
            <Route path="/products/:id" component={OneProductContainer}/>
            <Route path="/Register" component={RegisterContainer}/>
            <Route path="/logout" render={(props) => {return <Logout {...props} logout={() => this.props.logout()}/>}}/>
            <Redirect to='/'/>
        </Switch>
        return (
            <Header isLoggedIn={this.props.isLoggedIn}>
                {this.props.isLoggedIn
                    ? loggedRoutes
                    : NotloggedRoutes}
            </Header>)
    }
}

const mapStateToProps = state => {
    return {isLoggedIn: state.session.isLoggedIn};
}

const mapDispatchToProps = dispatch => {
    return {
        tokenCheck: () => dispatch(tokenCheck()),
        logout: () => dispatch(logout())
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App))