import * as actionName from '../actions/actionName'

const initialState = {
    products: {},
    currentProduct: {},
    redirect: false,
    loading: false,
    errors: false
}
export default (state = initialState, {type, payload}) => {
    switch (type) {
        case actionName.POST_START:
            return {
                ...state,
                loading: true
            }
        case actionName.POST_SUCCES:
            return {
                ...state,
                errors: false,
                redirect: true

            }
        case actionName.POST_FAILURE:
            return {
                ...state,
                loading: false,
                errors: true
            }
        case actionName.POST_END:
            return {
                ...state,
                loading: false,
                redirect: false
            }

        case actionName.INFO_GET_START:
        {
            return {
                ...state,
                currentProduct: null,
                loading: true,
                errors: false
            }
        }
        case actionName.INFO_GET_SUCCES:
        {
            return {
                ...state,
                loading: false,
                currentProduct: {
                    comments: payload
                }
            }
        }
        case actionName.INFO_GET_FAILURE:
        {
            return {
                ...state,
                loading: false,
                errors: payload
            }
        }
        default:
            return state
    }
}
