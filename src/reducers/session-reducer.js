import * as actionName from '../actions/actionName'

const initialState = {
  isLoggedIn: false,
  loading: false,
  redirect: false,
  errorMsgLog: '',
  errorMsgReg: '',
  error: '',
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case actionName.TOKEN_CHECK:
      if (localStorage.getItem('token')) {
        return {
          ...state,
          isLoggedIn: true,
        }
      }
      return state

    case actionName.LOG_START:
      return {
        ...state,
        loading: true,
        errors: false,
      }
    case actionName.LOG_IN_SUCCES: {
      localStorage.setItem('token', payload.payload)
      return {
        ...state,
        loading: false,
        isLoggedIn: true,
        redirect: true,
      }
    }

    case actionName.LOG_OUT: {
      localStorage.removeItem('token')
      return {
        ...state,
        isLoggedIn: false,
      }
    }

    case actionName.LOG_IN_FAILURE:
      return {
        ...state,
        loading: false,
        errors: { message: payload },
      }
    case actionName.LOG_IN_END:
      return {
        ...state,
        loading: false,
        redirect: false,
      }
    case actionName.REG_START:
      return {
        ...state,

        loading: true,
        errors: false,
        errorMsgReg: '',
      }

    case actionName.REG_SUCCES:
      localStorage.setItem('token', payload.payload)
      return {
        ...state,
        loading: false,
        isLoggedIn: true,
        redirect: true,
      }
    case actionName.REG_IN_FAILURE:
      return {
        ...state,
        errors: { message: payload },
      }
    case actionName.REG_IN_END:
      return {
        ...state,
        loading: false,
        redirect: false,
      }
    default:
      return state
  }
}
