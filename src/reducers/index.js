import { combineReducers } from 'redux'
import session from './session-reducer'
import info from './info-reducer'

export default combineReducers({
  session,info,
})
