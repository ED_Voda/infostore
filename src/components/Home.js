import React from "react";
import { Link } from 'react-router-dom';
import { httpGet } from "../helpers/network.js";


class Home extends React.Component{
    constructor(){
        super();
        this.state = {
            products: [],
        };
    };


    componentDidMount() {
        httpGet("products")
            .then(data => {
            let products = data.map((products) => {
                return(
                    <div key={products.id} className="card">
                        <h1 className="h1_prod"><Link className="link_main" to={`/products/${products.id}`}>{products.title}</Link></h1>
                        <div className="Main_cont_prod">
                            <img alt="" className="img_prod" src={`http://smktesting.herokuapp.com/static/${products.img}`}/>
                            <div className="p_prod">{products.text}</div>
                        </div>
                    </div>
                )
            });
            this.setState({products:products});
        })
    }



    render() {
        return (
            <div className="container">
                {this.state.products}
            </div>

        );
    }
}

export default Home
