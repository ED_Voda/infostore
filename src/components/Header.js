import React, {Component} from 'react'
import {Menu, Container,} from 'semantic-ui-react'
import {NavLink} from 'react-router-dom'
import LinkBtn from "./LinkBtn";

export class Header extends Component {
    render() {
        const {children, isLoggedIn} = this.props;

        //Меню если Не залогинен
        const NotLoggedMenu = <Menu pointing stackable size='large' className='main-menu'>
            <Menu.Item as={NavLink} to='/'>
                Каталог
            </Menu.Item>
            <Menu.Item position='right'>
                <LinkBtn to={'/login'} label={`Логин`}/>
            </Menu.Item>
        </Menu>
        //Меню если залогинен
        const loggedMenu = <Menu pointing stackable size='large' className='main-menu'>
            <Menu.Item as={NavLink} to='/'>
                Каталог
            </Menu.Item>
            <Menu.Item position='right'>
                <LinkBtn to={'/logout'} label={`Выйти`}/>
            </Menu.Item>
        </Menu>

        return (
            <div>

                <div className="background--image"></div>
                <Container>
                    {isLoggedIn
                        ? loggedMenu
                        : NotLoggedMenu}
                </Container>

                <Container style={{
                    marginTop: '60px'
                }}>
                    {children}
                </Container>

            </div>
        )
    }
}

export default Header