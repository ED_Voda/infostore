import React from 'react'
import PropTypes from 'prop-types'
import {Link, Redirect} from 'react-router-dom'

class Login extends React.Component {
  state = {
    redirectToPreviousRoute: false,
    username: '',
    password: '',
  }

  handleSubmit = e => {
    e.preventDefault()
    const { username, password } = this.state

    this.props.logIn(
      {
        username,
        password,
      }
    )

  }

  handleChange = e => {
    const value = e.currentTarget.value
    const fieldName = e.currentTarget.dataset.fieldName

    this.setState(prev => ({
      ...prev,
      [fieldName]: value,
    }))
  }

  render() {
      const { errorMsgLog, } = this.props
      const { from } = this.props.location.state || { from: { pathname: '/' } }
      const { username, password } = this.state
    if (this.props.redirect) {

      return <Redirect to={from} />
    }

    return (
      <div>
        {errorMsgLog && <p>{errorMsgLog}</p>}
        <form onSubmit={this.handleSubmit}>
          <input
            data-field-name={'username'}
            type={'text'}
            onChange={this.handleChange}
            placeholder={'Имя'}
            value={username}
          />
          <input
            data-field-name={'password'}
            type={'text'}
            onChange={this.handleChange}
            placeholder={'Пароль'}
            value={password}
          />
          <button type="submit">Log in</button>
            <p><Link to={`/Register`}>Регестрация</Link></p>
        </form>
      </div>
    )
  }
}

Login.propTypes = {
  logIn: PropTypes.func.isRequired,
  errorMsg: PropTypes.string,
}

export default Login
