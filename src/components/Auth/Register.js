import React from 'react'
import PropTypes from 'prop-types'
import {Link, Redirect} from 'react-router-dom'

class Register extends React.Component {
  state = {
    redirectToPreviousRoute: false,
    username: '',
    password: '',
  }

  handleSubmit = e => {
    e.preventDefault()
    const { username, password } = this.state

    this.props.Req(
      {
        username,
        password,
      },
      () => {
        this.setState({ redirectToPreviousRoute: true })
      }
    )
  }

  handleChange = e => {
    const value = e.currentTarget.value
    const fieldName = e.currentTarget.dataset.fieldName

    this.setState(prev => ({
      ...prev,
      [fieldName]: value,
    }))
  }

  render() {
    const { location, errorMsgReg } = this.props
    const { from } = location.state || { from: { pathname: '/' } }
    const { username, password } = this.state

    if (this.props.redirect) {
      return <Redirect to={from} />
    }

    return (
      <div>
        {errorMsgReg && <p>{errorMsgReg}</p>}
        <form onSubmit={this.handleSubmit}>
          <input
            data-field-name={'username'}
            type={'text'}
            onChange={this.handleChange}
            placeholder={'Имя'}
            value={username}
          />
          <input
            data-field-name={'password'}
            type={'text'}
            onChange={this.handleChange}
            placeholder={'Пароль'}
            value={password}
          />
          <button type="submit">Регестрация</button>
            <p><Link to={`/login`}>Войти</Link></p>
        </form>
      </div>
    )
  }
}

Register.propTypes = {
    Req: PropTypes.func.isRequired,
  errorMsg: PropTypes.string,
}

export default Register
