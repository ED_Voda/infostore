import React from "react";
import { Dimmer, Loader} from 'semantic-ui-react'
import {httpGet} from "../../helpers/network";
import CreateReviewContainer from "../../containers/CreateReviewContainer"

class Production_info extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            products: [],
            reviews:[],
        };
    };

    componentDidMount() {
        let id = this.props.match.params.id;
        let arr=[];
        httpGet("products")
            .then(data => {
                let product = data[id-1];
                arr.push(
                        <div key={product.id} className="card_info width-info">
                            <h1 className="h1_prod">{product.title}</h1>
                            <div className="Main_cont_prod">
                                <img alt="" className="img_prod" src={`http://smktesting.herokuapp.com/static//${product.img}`}/>
                                <p className="p_prod">{product.text}</p>
                            </div>
                        </div>
                );
                this.setState({products:arr});
            });

        httpGet(`reviews/${id}`)
            .then(data => {
                let reviews = data.map((reviews) => {
                    return(
                        <div key={reviews.id} className="card_reviews">
                            <div className="reviews_margin">
                                <div className="pp-review-date">
                                    <time>
                                        {reviews.created_at}
                                    </time>
                                </div>
                                <p><b>{reviews.created_by.username}</b>  Rate:{reviews.rate}</p>
                                <hr/>
                            </div>
                            <div className="reviews_margin">
                                {reviews.text}
                            </div>
                        </div>
                    )
                });

                this.setState({reviews:reviews.reverse()});
            });

    }

render() {
    const {loading} = this.props;
    const rend = <div>
        {this.state.products}
        <div className="review width-rev">
            <div>
                <h3>Оставить отзыв</h3>

                <CreateReviewContainer props={this.props}/>
            </div>
            <div>
                <h3>Отзывы покупателей</h3>
                {this.state.reviews}
            </div>
        </div>
    </div>
    const loader = loading && <Dimmer inverted active>
        <Loader inverted>Загрузка</Loader>
    </Dimmer>
    return (
        <div>
            {loader || rend}
        </div>
    );
}
};

export default Production_info


