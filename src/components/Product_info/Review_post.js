import React from 'react'
import PropTypes from 'prop-types'
import {Redirect} from "react-router-dom";

class Review_post extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: props.props.match.params.id,
            text: '',
            rate: ''
        }
    }


    handleSubmit = e => {
        e.preventDefault()
        const { text, rate, id } = this.state

        this.props.Rev(
            {
                text,
                rate,
                id
            }
        )
    }
    handleChange = e => {
        const value = e.currentTarget.value
        const fieldName = e.currentTarget.dataset.fieldName

        this.setState(prev => ({
            ...prev,
            [fieldName]: value,
        }))
    }

    rendLogin = () => {
        const { text } = this.state
        return (
            <div className="newReview" >
                <div className="star-rating">
                    <div className="star-rating__wrap">
                        <input className="star-rating__input" id="star-rating-5" type="radio" name="rating" value="5" onClick={this.handleChange} data-field-name={'rate'}/>
                        <label className="star-rating__ico fa fa-star-o fa-lg" htmlFor="star-rating-5" title="5 out of 5 stars"></label>
                        <input className="star-rating__input" id="star-rating-4" type="radio" name="rating" value="4" onClick={this.handleChange} data-field-name={'rate'}/>
                        <label className="star-rating__ico fa fa-star-o fa-lg" htmlFor="star-rating-4" title="4 out of 5 stars"></label>
                        <input className="star-rating__input" id="star-rating-3" type="radio" name="rating" value="3" onClick={this.handleChange} data-field-name={'rate'}/>
                        <label className="star-rating__ico fa fa-star-o fa-lg" htmlFor="star-rating-3" title="3 out of 5 stars"></label>
                        <input className="star-rating__input" id="star-rating-2" type="radio" name="rating" value="2" onClick={this.handleChange} data-field-name={'rate'}/>
                        <label className="star-rating__ico fa fa-star-o fa-lg" htmlFor="star-rating-2" title="2 out of 5 stars"></label>
                        <input className="star-rating__input" id="star-rating-1" type="radio" name="rating" value="1" onClick={this.handleChange} data-field-name={'rate'}/>
                        <label className="star-rating__ico fa fa-star-o fa-lg" htmlFor="star-rating-1" title="1 out of 5 stars"></label>
                    </div>
                </div>
                <textarea data-field-name={'text'} id="content" placeholder="Оставьте отзыв..." className="form-control reviews_margin" onChange={this.handleChange} value={text}></textarea>
                <button className="btn btn-info submit-btn " type="submit" onClick={this.handleSubmit}>Submit Review</button>
            </div>
        )
    };

    rendNotLogin = () =>{
        return(
            <div className="newReview">
                <div className="star-rating">
                    <div className="star-rating__wrap">
                        <input className="star-rating__input" id="star-rating-5" type="radio" name="rating" value="5"/>
                        <label className="star-rating__ico fa fa-star-o fa-lg" htmlFor="star-rating-5" title="5 out of 5 stars"></label>
                        <input className="star-rating__input" id="star-rating-4" type="radio" name="rating" value="4"/>
                        <label className="star-rating__ico fa fa-star-o fa-lg" htmlFor="star-rating-4" title="4 out of 5 stars"></label>
                        <input className="star-rating__input" id="star-rating-3" type="radio" name="rating" value="3"/>
                        <label className="star-rating__ico fa fa-star-o fa-lg" htmlFor="star-rating-3" title="3 out of 5 stars"></label>
                        <input className="star-rating__input" id="star-rating-2" type="radio" name="rating" value="2"/>
                        <label className="star-rating__ico fa fa-star-o fa-lg" htmlFor="star-rating-2" title="2 out of 5 stars"></label>
                        <input className="star-rating__input" id="star-rating-1" type="radio" name="rating" value="1"/>
                        <label className="star-rating__ico fa fa-star-o fa-lg" htmlFor="star-rating-1" title="1 out of 5 stars"></label>
                    </div>
                </div>
                <textarea id="content " placeholder="Что бы оставлять отзыв вы должны быть зарегестрированы!" className="form-control reviews_margin" disabled ></textarea>
            </div>
        );
    }

    render() {
        if (this.props.redirect) {

            return <Redirect to={`/products/${this.id}`} />
        }
        if(this.props.isLoggedIn){
            return this.rendLogin()
        }else{
            return this.rendNotLogin()
        }
    }
}

Review_post.proptypes = {
    user: PropTypes.shape({
        name: PropTypes.string.isRequired,
    }).isRequired,
}

export default Review_post